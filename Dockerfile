FROM golang:1.15-alpine3.13

WORKDIR /app

COPY . .

RUN go build -o ./dist/main ./cmd/data-publisher

CMD ["/app/dist/main"]
