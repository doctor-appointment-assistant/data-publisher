down:
	docker-compose down
build:
	docker-compose build
up:
	docker-compose up
up-build:
	docker-compose up --build
re: down up-build
