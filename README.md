# Doctor Appointment Assistance

Please refer to the `Makefile` for convenient functions that you may use.

Also, consider looking at `docker-compose.yaml` as it contains information about environment variables needed to be set.
