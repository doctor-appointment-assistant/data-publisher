package main

import (
	"log"

	"github.com/Netflix/go-env"
	"github.com/go-redis/redis/v8"
)

type Environment struct {
	RedisHost string `env:"REDIS_HOST,required=true"`
	// RedisPass string `env:"REDIS_PASS,required=true"`

	PatientsMax       int `env:"PATIENTS_MAX,required=true"`
	PatientsMin       int `env:"PATIENTS_MIN,required=true"`
	MaxChangePerRound int `env:"MAX_CNGE_PER_RND,required=true"`

	PrintEveryS int `env:"PRINT_EVERY_S,required=true"`

	Extra env.EnvSet
}

var Env *Environment = &Environment{}
var Redis *redis.Client

func init() {
	es, err := env.UnmarshalFromEnviron(Env)
	if err != nil {
		log.Fatalln(err)
	}

	Env.Extra = es

	initDatabase()
}

func initDatabase() {
	Redis = redis.NewClient(&redis.Options{
		Addr: Env.RedisHost,
		// Password: Env.RedisPass,
		DB: 0,
	})
}
