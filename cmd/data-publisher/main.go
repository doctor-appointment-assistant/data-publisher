package main

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var ctx, cancelCtx = context.WithCancel(context.Background())

func main() {
	fmt.Println("Hello, world!")

	go dataProducingLoop()
	go dataPrintingLoop()

	var captureSignal = make(chan os.Signal, 1)
	signal.Notify(captureSignal, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
	<-captureSignal

	cancelCtx()
	fmt.Println("Context Cancelled")

	os.Exit(0)
}

func dataProducingLoop() {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	currPatients := r.Intn(Env.PatientsMax-Env.PatientsMin+1) + Env.PatientsMin

	for {
		prob := r.Float32()
		change := r.Intn(Env.MaxChangePerRound + 1)
		if prob >= 0.5 {
			currPatients += change

			if currPatients > Env.PatientsMax {
				currPatients = Env.PatientsMax
			}
		} else {
			currPatients -= change

			if currPatients < Env.PatientsMin {
				currPatients = Env.PatientsMin
			}
		}

		err := Redis.Set(ctx, "currentPatients", currPatients, 0).Err()
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error saving currentPatients:", err)
		}

		time.Sleep(time.Duration(100 * r.Float32() * float32(time.Millisecond)))
	}
}

func dataPrintingLoop() {
	for {
		time.Sleep(time.Duration(Env.PrintEveryS) * time.Second)

		val, err := Redis.Get(ctx, "currentPatients").Result()
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error reading currentPatients:", err)
		} else {
			fmt.Println("currentPatients:", val)
		}
	}
}
