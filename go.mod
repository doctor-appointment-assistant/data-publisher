module gitlab.com/doctor-appointment-assistant/data-publisher

go 1.15

require (
	github.com/Netflix/go-env v0.0.0-20210215222557-e437a7e7f9fb
	github.com/go-redis/redis/v8 v8.8.2
)
